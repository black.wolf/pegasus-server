using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace PegasusServer
{
    // ReSharper disable once ClassNeverInstantiated.Global
    internal class DaemonHostedService: IHostedService
    {
        internal static readonly string ServerDesc =  $"Pegasus C# Chat Server {Assembly.GetExecutingAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion}, build {Assembly.GetExecutingAssembly().GetName().Version}";
        
        private readonly ILogger<DaemonHostedService> _logger;  
        private readonly IHostApplicationLifetime _appLifetime;
        private readonly IConfiguration _configuration;  
        private readonly CancellationTokenSource _tokenSource = new CancellationTokenSource();
        private ChatServer _chatServer;
        
        public DaemonHostedService(IConfiguration configuration, ILogger<DaemonHostedService> logger, IHostApplicationLifetime appLifetime)  
        {  
            _configuration = configuration;  
            _logger = logger;  
            _appLifetime = appLifetime;
        }  
  
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation(ServerDesc);
            
            try
            {
                var port = _configuration.GetValue<int?>("port") ?? 12345; 
                Task.Run((_chatServer = new ChatServer(port, _tokenSource.Token, _logger)).ServerLoop)
                    .ContinueWith(_ => _appLifetime.StopApplication());
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error starting server!");
                _appLifetime.StopApplication();
            }
            
            return Task.CompletedTask;  
        }  
  
        public Task StopAsync(CancellationToken cancellationToken)  
        {  
            _logger.LogInformation("Server tearing down...");
            _tokenSource.Cancel();
            try
            {
                _chatServer?.ShutdownAllClients().Wait(cancellationToken);
            }
            catch (OperationCanceledException)
            {
                _logger.LogError("Teardown failed to detach all clients on time.");
            }
            
            _logger.LogInformation("Server teardown completed. Awaiting process end.");
            return Task.CompletedTask;  
        }  
    }
}