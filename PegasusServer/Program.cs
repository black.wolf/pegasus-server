﻿using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

namespace PegasusServer
{
    internal static class Program
    {
        private static async Task Main(string[] args)
        {
            var host = new HostBuilder()
                //.ConfigureHostConfiguration(configHost => configHost.AddCommandLine(args))
                .ConfigureAppConfiguration((hostContext, configApp) =>  configApp.AddCommandLine(args))
                .ConfigureServices((hostContext, services) =>  
                {  
                    services.AddLogging();
                    services.Configure<ConsoleLifetimeOptions>(options => options.SuppressStatusMessages = true);
                    services.AddHostedService<DaemonHostedService>();  
                })  
                .ConfigureLogging((hostContext, configLogging) =>
                {
                    configLogging.AddSerilog(new LoggerConfiguration().WriteTo.Console().CreateLogger());  
                    configLogging.AddDebug();
                })  
                .Build();  
  
            await host.RunAsync();  
        }  
    }
}