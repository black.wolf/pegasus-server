using System;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PegasusServer
{
    public class ChatUser
    {
        private readonly TcpClient _tcpConn;
        private readonly CancellationToken _tokenOfDeath;
        private readonly StreamWriter _nsw;
        private readonly StreamReader _nsr;

        internal string Username { get; private set; }

        private ChatUser(TcpClient tcpConn, CancellationToken tokenOfDeath)
        {
            _nsw = new StreamWriter(tcpConn.GetStream(), Encoding.ASCII) { AutoFlush = true };
            _nsr = new StreamReader(tcpConn.GetStream(), Encoding.ASCII);
            _tcpConn = tcpConn;
            _tokenOfDeath = tokenOfDeath;
        }
        
        public bool IsAlive {
            get
            {
                try
                {
                    return !_tcpConn.Client.Poll(1, SelectMode.SelectRead) || _tcpConn.GetStream().DataAvailable;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static async Task<(string user, ChatUser chatter)> Register(TcpClient sock, CancellationToken tokenOfDeath)
        {
            var chatter = new ChatUser(sock, tokenOfDeath);

            try
            {
                await chatter._nsw.WriteLineAsync(DaemonHostedService.ServerDesc);
                await chatter._nsw.WriteLineAsync("Welcome to Pegasus Chat server!");
                while (true)
                {
                    await chatter._nsw.WriteAsync("\x1b[31;1m[pegasus]\x1b[0m Identify yourself: ");
                    var user = await chatter._nsr.ReadLineAsync();
                    if (string.IsNullOrEmpty(user) || user == "pegasus")
                    {
                        await chatter.SendMessage("pegasus", "We do not accept anonymous users nor wiseguys. Choose a name!");
                        continue;
                    }

                    await chatter.SendMessage("pegasus", $"Welcome, \x1b[32m{user}\x1b[0m! Use :quit to leave.", true);
                    return (user, chatter);
                }
            }
            catch (SocketException)
            {
                await chatter.ShutdownUser(false);
                return (null, null);
            }
        }

        internal async Task SendMessage(string user, string message, bool isAdmin = false)
        {
            const string ansiInsert = "\x1b[s\x1b[G\x1b[L";
            const string ansiPost = "\x1b[u";

            try
            {
                await _nsw.WriteLineAsync($"{ansiInsert}\x1b[{(isAdmin ? "31" : "32")};1m[{user}]\x1b[0m {message}{ansiPost}");
            }
            catch (Exception)
            {
                await ShutdownUser(false);
            }
        }

        public async Task ShutdownUser(bool gracefully = true)
        {
            if (gracefully && _tcpConn.Connected) await _nsw.WriteLineAsync("Connection gracefully closing.");
            _tcpConn.Close();
        }

        public async Task Loop(string user, Func<string, string, bool, Task> broadcastToAll)
        {
            Username = user;
            while (!_tokenOfDeath.IsCancellationRequested)
            {
                await _nsw.WriteAsync("> ");
                var cmd = await _nsr.ReadLineAsync();
                if (cmd?.ToLower() == ":quit") break;
                await broadcastToAll(Username, cmd, false);
            }

            await ShutdownUser();
        }
    }
}