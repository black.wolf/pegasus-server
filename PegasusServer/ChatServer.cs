using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace PegasusServer
{
    internal class ChatServer
    {
        private readonly CancellationToken _tokenOfDeath;
        private readonly ILogger _logger;
        private readonly int _port;
        private TcpListener _server;
        private Timer _timer;

        private ConcurrentDictionary<string, ChatUser> Chatters {get;} = new ConcurrentDictionary<string, ChatUser>();

        internal ChatServer(int port, in CancellationToken tokenOfDeath, in ILogger logger)
        {
            _port = port;
            _tokenOfDeath = tokenOfDeath;
            _logger = logger;
        }

        internal async Task ServerLoop()
        {
            try
            {
                _logger.LogInformation($"Initiating listening socket on port {_port}");
                _server = new TcpListener(IPAddress.Any, _port);

                // ReSharper disable once ImpureMethodCallOnReadonlyValueField
                _tokenOfDeath.Register(() =>
                {
                    _logger.LogInformation("Main server loop stopping.");
                    _server?.Stop();
                    _timer?.Dispose();
                    _logger.LogInformation("Stopped listening for new connections.");
                });            
            
                _server.Start();
                _timer = new Timer(CheckTheLiving, null, TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(2));

                _logger.LogInformation("Init complete, awaiting client connections...");
                
                while (!_tokenOfDeath.IsCancellationRequested)
                    await _server.AcceptTcpClientAsync().ContinueWith(async tcpClient => await RegisterClient(tcpClient));
            }
            catch (Exception e)
            {
                _logger.LogError("Error in main server loop, aborting server.", e);
            }
        }

        private void CheckTheLiving(object dummy)
        {
            var living = Chatters.Values.ToArray();
            var checkGroup = living.Select(CheckUserAlive).ToArray();
            Task.WaitAll(checkGroup);
        }

        private async Task CheckUserAlive(ChatUser u)
        {
            if (!u.IsAlive && Chatters.TryRemove(u.Username, out _))
            {
                _logger.LogInformation($"User {u.Username} disconnected");
                await BroadcastToAll("pegasus", $"User \x1b[33m{u.Username}\x1b[0m left the chat. Talk behind his back...", true);
                await u.ShutdownUser();
            }
        }

        internal async Task ShutdownAllClients() => await Task.WhenAll(Chatters.Values.Select(c => c.ShutdownUser()));

        private async Task BroadcastToAll(string user, string message, bool isAdmin = false) => await Task.WhenAll(Chatters.Values.Select(c => c.SendMessage(user, message, isAdmin)));

        private async Task RegisterClient(Task<TcpClient> connectionTask)
        {
            var tcpClient = await connectionTask;
            _logger.LogInformation($"Incoming connection from {tcpClient.Client.RemoteEndPoint}");

            var (user, chatter) = await ChatUser.Register(tcpClient, _tokenOfDeath);
            if (user == null)
            {
                _logger.LogInformation($"User from {tcpClient.Client.RemoteEndPoint} dropped before identifying");
                tcpClient.Close();
                return;
            }

            var x = 0;
            var newUser = user;
            while (Chatters.GetOrAdd(newUser, chatter) != chatter) newUser = $"{user}{++x}";
            if (x > 0) await chatter.SendMessage("pegasus", $"Your username was taken so we renamed you to {newUser}. Deal with it.", true);

            _logger.LogInformation($"User from {tcpClient.Client.RemoteEndPoint} identified as {newUser}");
            await BroadcastToAll("pegasus", $"User \x1b[33m{newUser}\x1b[0m joined the chat. Be nice. Or don't, I don't care...", true);
            await Task.Run(() => chatter.Loop(newUser, BroadcastToAll));
        }
    }
}